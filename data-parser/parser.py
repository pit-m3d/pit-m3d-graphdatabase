from gql_client import GQL
from xml.etree import ElementTree as ET
import argparse
from xml.etree.ElementTree import Element
import os
import json
import glob
import logging
import ast


class XMLParser:

    def set_arguments(self):
        parser = argparse.ArgumentParser(description= "Add necessary arguments.")
    

        parser.add_argument('--status', metavar='status', help = 'Is the data clean? Yes or No', type= bool,
        default=True)

        parser.add_argument('--path', metavar= '--path', type =str,
        help='Full path of the data/folder being kept', required=True)
        
        parser.add_argument('--connection', metavar= '--connection', type =str,
        help='Connection port to Graphql server. Default http://0.0.0.0:3003/', default="http://0.0.0.0:3003/")

        args = parser.parse_args()
        self.path = args.path
        self.clean = args.status
        self.gql_server = GQL(url= args.connection)

    def set_connection(self, url = "http://0.0.0.0:3003/"):
        self.gql_server = GQL(url= url)

    def parse_clean_metabolite(self, xml,filename):
    
        root = xml.getroot()
        properties = {}

        #Get all the basic metabolite properties
        for tag in root:
            if tag.tag == 'ranges':
                continue
            
            properties[tag.tag] = tag.text

        properties['source'] = self.get_source(filename)

        flag, met_id = self.gql_server.mutation_update_node("CreateMetabolite", properties)
        
        if not flag:
            print(met_id)


        metabolite_uuid = self.gql_server.get_uuid('Metabolite', properties['name'].strip())[1]
        self.connect_metabolite_disease(root.find('ranges'), metabolite_uuid)

        return flag, met_id

    def connect_metabolite_disease(self,ranges, metabolite_uuid):

        for range in ranges:
            properties = {}
            flag = True
            for child in range:

                if child.tag != 'disease':
                    if child.tag == 'age':
                        properties['minAge'] = child.find('min').text
                        properties['maxAge'] = child.find('max').text
                    else:
                        properties[child.tag] = child.text

            disease = range.find('disease')
            disease_name_tag = disease.find('name')
            disease_uuid = ""

            if disease_name_tag.text.strip() != 'Normal':
                flag, disease_uuid = self.gql_server.get_uuid('Disease', disease_name_tag.text.strip())
                properties['indicator'] = disease.find('indicator').text
            
            if not flag:
                continue

            flag, id = self.gql_server.connect_metabolite_disease(disease_uuid, metabolite_uuid, properties)
            
            if not flag:
                print(id)


    def create_disease(self, root, filename):
        properties = {}

        for tag in root:
            if tag.tag in ['creation_date', 'update_date', 'name', 'version']:
                properties[tag.tag] = tag.text

        properties['source'] = self.get_source(filename)

        flag, id = self.gql_server.mutation_update_node("CreateDisease", properties)

        if not flag:
            return flag, id

        return flag, id

    def attach_disease_properties(self, root):
        for tag in root:
            for child in tag:
                if child.tag == 'link':
                    self.create_disease_link_properties(child, id)
                else:
                    break
            
            # if tag.tag == 'icd' and 'path' in tag.attrib:
            #     self.create_icd_path(tag, id)

            if tag.tag == 'references':
                self.create_references()
            
            if tag.tag == 'symptoms':
                self.create_symptoms()

    def parse_clean_disease(self, xml, filename):
        root = xml.getroot()
        flag, id = self.create_disease(root, filename)

        if not flag:
            print(id)


    def create_references(self,xml, id):

        for reference in xml:
            properties = {}

            for attrib in reference.attrib:
                properties[attrib] = reference.attrib[attrib]

            for child in reference:
                properties[child.tag] = child.text

            ref_id = self.gql_server.mutation_update_node("CreateReference", properties)['data']['CreateReference']['_id']

            params = {"from": int(id), "to": int(ref_id), "typeFrom": "Disease", "typeTo": "Reference",
            "relType": 'DISEASE_PROPERTY', 'name': "reference"}

            try:
                self.gql_server.query(params)
            except:
                print("Could not link reference with disease!")
            


        return None

    def create_symptoms(self, xml, id):

        for symptom in xml:
            properties = {}

            for attrib in symptom.attrib:
                properties[attrib] = symptom.attrib[attrib]

            properties['name'] = symptom.find('name').text.strip()
            properties['category'] = symptom.find('category').text.strip()

            symptom_id = self.gql_server.mutation_update_node("CreateSymptom", properties)['data']['CreateSymptom']['_id']
            try:
                self.gql_server.query({"from": int(id), 'to': int(symptom_id), "typeFrom" : 'Disease',
            'typeTo': "Symptom", 'relType' : 'DISEASE_PROPERTY', 'name': 'synonym'})
            except:
                print('Could not create relationship for symptom')


    def create_icd_path(self, tag, id):
        
        icd_path = ast.literal_eval(tag.attrib['path'])
        source = tag.attrib['source'].strip()


        return None

    def create_disease_link_properties(self, xml, id):

        properties = {}

        if 'source' in xml.attrib:
            properties['source'] = xml.attrib['source']

        if 'url' in xml.attrib:
            properies['url'] = xml.attrib['url']
        

        properties['data'] = xml.text

        link_id = self.gql_server.mutation_update_node("CreateLink", properties)
        params = {"from": int(id), "to": int(link_id), "typeFrom": 'Disease', 'typeTo': 'Link', 
        "name": xml.tag, 'relType' : 'DISEASE_PROPERTY'}

        try:
            rel_id = self.gql_server.query(params)
            return True, rel_id
        except:
            print('Could not create relationship')
            return False, None
        

    def get_source(self, file):
        filename = file.lower()

        if "hmdb" in filename or filename.split('.')[0].isnumeric():
            return ["HMDB"]
        elif 'mathmeta' in filename or 'diemet' in filename:
            return ["META"]
        elif 'csv' in filename:
            return ['CSV']
        elif 'mathmc' in filename or 'diehc' in filename:
            return [ "HMDB", "CSV"]
        elif 'mathhmcm' in filename or 'diehmc' in filename:
            return ['HMDB', 'METAGENE', 'CSV']
        else:
            return []


    def parse(self):
        if self.path.endswith('.xml'):
            xml = ET.parse(path)
            print("start parsing file " + os.path.basename(path))
            if path.endswith('Diseases'):
                self.parse_clean_disease(xml, os.path.basename(self.path))
            else:
                self.parse_clean_metabolite(xml, os.path.basename(self.path))

        elif self.path.endswith('.zip'):
            return None

        else:
            print("Start with disease files\n")
            for filepath in glob.glob(self.path+"/Diseases/*.xml"):
                xml = ET.parse(filepath)
                print("start parsing file " + os.path.basename(filepath))
                self.parse_clean_disease(xml, os.path.basename(filepath))
            
            print("Start with metabolite files\n")
            for filepath in glob.glob(self.path+"/Metabolites/*.xml"):
                xml = ET.parse(filepath)
                print("start parsing file " + os.path.basename(filepath))
                self.parse_clean_metabolite(xml, os.path.basename(filepath))



if __name__ == '__main__':
    
    xml_parser = XMLParser()
    xml_parser.set_arguments()
    xml_parser.parse()

