import ParsingEditor from './components/Editor/ParsingEditor';
import React from 'react';

function App() {
  return (
    <div className="App">
      <ParsingEditor />
    </div>
  );
}

export default App;
