import { Colors, Controls, FlumeConfig } from 'flume';
// NOTE: Root node must go at the bottom of the config
const config = new FlumeConfig();

config
  .addPortType({
    type: 'gqlQuery',
    name: 'gqlQuery',
    label: 'GraphQL Queries',
    color: Colors.purple,
  })
  .addPortType({
    type: 'string',
    name: 'string',
    label: 'String',
    color: Colors.grey,
    controls: [
      Controls.text({
        name: 'string',
        label: 'Tag name',
      }),
    ],
    hidePort: true,
  })
  .addPortType({
    type: 'element',
    name: 'element',
    label: 'XML Element',
    color: Colors.green,
  })
  .addNodeType({
    type: 'matchElementByTag',
    label: 'Match XML Element',
    description: 'Matches an XML element by its tag.',
    inputs: (ports: any) => [ports.string()],
    outputs: (ports: any) => [ports.element()],
  })
  .addRootNodeType({
    type: 'runGraphQL',
    label: 'Push Changes',
    initialWidth: 170,
    inputs: (ports: any) => [ports.gqlQuery()],
  });

export default config;
