import { NodeEditor } from 'flume';
import React from 'react';
import config from './config';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  container: {
    width: 1200,
    height: 900,
  },
});

function ParsingEditor() {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <NodeEditor
        portTypes={config.portTypes}
        nodeTypes={config.nodeTypes}
        defaultNodes={[
          {
            type: 'runGraphQL',
            x: 190,
            y: -150,
          },
        ]}
      />
    </div>
  );
}

export default ParsingEditor;
