import '@fontsource/inter';

import { ThemeProvider, createMuiTheme } from '@material-ui/core';

import App from './App';
import React from 'react';
import ReactDOM from 'react-dom';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Inter',
  },
});

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
