# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


### Running the parser

To parse data in NEO4j database, you need to run the *parser.py* with the path of the folder you keep the data.

The structure of the data is the following:

* set up a folder you want to put the data in
* create a folder called *Metabolites* and put the metabolite xml data in there
* create another folder called *Diseases* folder and put the disease xml data in there
* run the parser.py. Example is: **python parser --path path/to/data/folder**