import argparse
import os
import tqdm
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element
from py2neo import Graph, Node, Relationship, NodeMatcher, RelationshipMatcher
from NEO4J import NEO4J
from parser_helper import XMLHelper

special_tags = ['synonyms', 'ontology', 'experimental_properties', 'predicted_properties', 'spectra',
    'biological_properties', 'protein_associations', 'ranges', '',
    'normal_concentrations', 'abnormal_concentrations', 'diseases', 'general_references']
class XMLParser:

    neo = NEO4J()

    def set_arguments(self):
        parser = argparse.ArgumentParser(description= "Add necessary arguments.")
    
        parser.add_argument('--source', metavar= 'source', type= str, 
        help= 'Give the data source. Default is HMDB.', default="HMDB",
        choices=["METAGENE", "HMDB", 'CSV', 'MIX'])
        
        parser.add_argument('--data_type', metavar='datatype', type = str,
        help= 'Give the data type. Default is metabolite', choices= ["metabolite", "disease"],
        default='metabolite')

        parser.add_argument('--status', metavar='status', help = 'Is the data clean?', type= bool,
        default=True)

        parser.add_argument('--path', metavar= '--path', type =str,
        help='Relative or full path of the data being kept', required=True)
        
        parser.add_argument('--connection', metavar= '--connection', type =str,
        help='Connection port to database', default="bolt://192.168.0.136")
        
        parser.add_argument('--password', metavar= '--password', type =str,
        help='Password to the database', default='test')

        parser.add_argument('--username', metavar= '--username', type =str,
        help='Username to the database', default='test')

        parser.add_argument('--omit',nargs='+', 
        help= "Tags that are omitted from parsing", default=[])

        args = parser.parse_args()


        self.source = args.source
        self.omit = args.omit
        self.data_type = args.data_type
        self.path = args.path
        self.clean = args.status


        self.neo.connect(args.connection, args.username, args.password)
        XMLHelper.neo = self.neo
        XMLHelper.source = self.source

    def parse(self):


        for file in os.listdir(self.path):

            xml = ET.parse(self.path + '/' + file)
           
            if self.data_type == 'metabolite':
                self.parse_metabolite(xml)
            elif self.data_type == 'disease':
                self.parse_disease(xml)


    def parse_disease(self, xml):

        return None

    def parse_metabolite(self, xml):

        root = xml.getroot()
        label = self.source+'Metabolite'
        basic_properties = {}

        #Get all the basic metabolite properties
        for tag in root:

            if tag.tag not in self.omit and tag.tag not in special_tags:
                text = tag.text
                if text is None:
                    text = 'NA'
                else:
                    text = text.strip()
                basic_properties[tag.tag] = text

        #Check if node already exist
        flag, node = self.neo.check_node_exist(label, {'name': basic_properties['name']})

        if not flag:        #If node doesn't exist, create one
            node = self.neo.create_node(label, basic_properties)
        
        #Add special properties
        self.add_special_tag_metabolite(node, xml)



    def add_special_tag_metabolite(self, node, xml):

        for tag in special_tags:
            xml_element = xml.find(tag)
            if tag in self.omit or xml_element is None:
                continue

            if tag == 'synonyms':
                XMLHelper.create_synonym(node, xml_element)
            elif tag == 'taxonomy':
                XMLHelper.create_taxonomy(node, xml_element)
            elif tag == 'experimental_properties':
                XMLHelper.create_properties(node, xml_element)
            elif tag == 'predicted_properties':
                XMLHelper.create_properties(node, xml_element)
            elif tag == 'spectra':
                XMLHelper.create_properties(node, xml_element)
            elif tag == 'biological_properties':
                XMLHelper.create_biological_properties(node, xml_element)
            elif tag == 'normal_concentrations':
                XMLHelper.create_concentrations(node, xml_element)
            elif tag == 'abnormal_concentrations':
                XMLHelper.create_concentrations(node, xml_element)
            elif tag == 'general_references':
                XMLHelper.create_general_references(node, xml_element)
            # elif tag == 'diseases' and self.clean:
            #     XMLHelper.create_diseases(node, self.clean, xml_element)
            elif tag == 'ranges':
                XMLHelper.create_ranges(node, xml_element)
            elif tag == 'protein_associations':
                XMLHelper.create_protein(node, xml_element)
            elif tag == 'secondary_accessions':
                XMLHelper.create_secondary_accessions(node, xml_element)



if __name__ == '__main__':

    xml_parser = XMLParser()
    xml_parser.set_arguments()
    #xml_parser.parse()

    xml = ET.parse('hmdb_metabolites-05.xml')
    xml_parser.parse_metabolite(xml)
