import os
import tqdm
from xml.etree import ElementTree as ET
from py2neo import Graph, Node, Relationship, NodeMatcher, RelationshipMatcher
from xml.etree.ElementTree import Element
from NEO4J import NEO4J


class XMLHelper:


    neo = NEO4J()
    source = "HMDB"

    @staticmethod
    def create_synonym(node, xml):
        
        synonyms = xml.findall('synonym')

        for synonym in synonyms:
            flag, synonym_node = XMLHelper.neo.check_node_exist(label = XMLHelper.source+'Synonym',
             properties={'name' : synonym.text.strip()})
            
            if not flag:
                synonym_node = XMLHelper.neo.create_node(XMLHelper.source+'Synonym',{'name': synonym.text.strip()})

            flag, _ = XMLHelper.neo.check_relationship_exist(node, synonym_node, 'SYNONYM')

            if not flag:
                XMLHelper.neo.create_relationship(node, synonym_node, 'SYNONYM')


    @staticmethod
    def create_taxonomy(node, xml):

        properties = {}
        special_tags = []

        for xml_tag in xml:

            if len(xml_tag) == 0:
                properties[xml_tag.tag] = xml_tag.text.strip()
            
            else:
                special_tags.append(xml_tag)

        taxonomy_node = XMLHelper.neo.create(node(XMLHelper.source+'Taxomony', properties))
        XMLHelper.neo.create_relationship(node, taxonomy_node, 'TAXOMONY')

        for xml_tag in special_tags:
            for child in xml_tag:
                child_node = XMLHelper.neo.create_node(child.tag, {'name': child.text.strip().upper()})
                XMLHelper.neo.create_relationship(taxonomy_node, child_node, xml_tag.tag.upper())

    @staticmethod
    def create_reference_list(xml):
        reference_text = []
        reference_id = []

        for reference in xml:
            reference_text.append(reference.find('reference_text').text.strip())
            pubmed_id = reference.find('pubmed_id')
            if pubmed_id is None:
                reference_id.append('NA')
            else:
                reference_id.append(pubmed_id.text.strip())

        return reference_text, reference_id

    @staticmethod
    def create_properties(node, xml):

        for property in xml:

            property_node = XMLHelper.neo.create_node(property.tag.title())
            child_properties = {}
            for tag in property:
                text = tag.text
                if text is None:
                    text = 'NA'
                child_properties[tag.tag] = text.strip()

            XMLHelper.neo.modify_node_properties(properties = child_properties, node = property_node)
            XMLHelper.neo.create_relationship(node, property_node, xml.tag.upper())

    @staticmethod
    def create_biological_properties(node, xml):
        for xml_tag in xml:
            if xml_tag.tag != 'pathways':
                for child in xml_tag:
                    child_node = XMLHelper.neo.create_node(xml_tag.tag.title(), {child.tag : child.text.strip()})
                    XMLHelper.neo.create_relationship(node, child_node, xml.tag.upper())

        for child_tag in xml.find('pathways'):
            properties = {}
            for child in child_tag:
                properties[child.tag] = child.text.strip()

            pathway_node = XMLHelper.neo.create_node(child_tag.title(), properties)
            XMLHelper.neo.create_relationship(node, pathway_node, xml.tag.upper())

    @staticmethod
    def create_concentrations(node, xml):
        for concentration in xml:
            properties = {}
            reference_text = []
            reference_id = []
            for child in concentration:
                if child.tag == 'references':
                    reference_text, reference_id = XMLHelper.create_reference_list(child)
                else:
                    properties[child.tag] = child.text.strip()

            properties['reference_text'] = reference_text
            properties['reference_pubmed_id'] = reference_id
            normal_conc = XMLHelper.neo.create_node(concentration.tag.title(), properties)
            XMLHelper.neo.create_relationship(node, normal_conc, xml.tag.upper())
  

    @staticmethod
    def create_general_references(node, xml):
        reference_text, reference_id = XMLHelper.create_reference_list(xml)
        XMLHelper.neo.modify_node_properties({'reference_text' : reference_text, 'pubmed_id' : reference_id},node)

    @staticmethod
    def create_ranges(node, xml):
        
        for range in xml:
            relationship_properties = range.attrib
            disease_name = None
            for tag in range:
                if tag.tag != 'disease':
                    relationship_properties[tag.tag] = tag.text.strip()
                else:
                    reference = tag.find('references').find('reference')
                    relationship_properties['reference_text'] = reference.find('reference_text').text.strip()
                    relationship_properties['pubmed_id'] = reference.find('pubmed_id').text.strip()
                    disease_name = tag.find('name').text.strip()

                    if tag.find('indicator') is not None:
                        relationship_properties['indicator'] = tag.find('indicator').text.strip()
                    else:
                        relationship_properties['indicator'] = 'NA'

            flag, disease_node = XMLHelper.neo.check_node_exist(XMLHelper.source+'Disease', {'name': disease_name})

            if not flag:
                disease_node = XMLHelper.neo.create_node(XMLHelper.source+'Disease',{'name': disease_name})
                XMLHelper.neo.create_relationship(node, disease_node, 'DISEASE', relationship_properties)
            else:
                flag_rel, relationship_dis = XMLHelper.neo.check_relationship_exist(node, disease_node, 
                    label=XMLHelper.source+'Disease',properties={'name': disease_name})

                if not flag_rel:
                    XMLHelper.neo.create_relationship(node, disease_node, 'DISEASE', relationship_properties)
                else:
                    XMLHelper.neo.modify_edge_properties(relationship_properties, relationship_dis)


    @staticmethod
    def create_protein(node, xml):
        for protein in xml:
            properties = {}
            for child in protein:
                properties[child.tag] = child.text.strip()
            protein_node = XMLHelper.neo.create_node(protein.tag, properties)
            XMLHelper.neo.create_relationship(node, protein_node, xml.tag.upper())

    @staticmethod
    def create_secondary_accessions(node, xml):
        list_accesssions = []
        for accession in xml:
            list_accesssions.append(accession.text.strip())
        
        XMLHelper.neo.modify_node_properties({'secondary_accessions':list_accesssions}, node)