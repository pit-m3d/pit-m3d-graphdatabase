import json
import os
import logging
import sys
from py2neo import Graph, Node, NodeMatcher, Relationship, RelationshipMatcher


class NEO4J:

    graph = None
    connection = ""
    username = ""
    password = ""
    
    def connect(self, connection= "bolt://192.168.0.136", username= 'neo4j', password= 'test'):
    
        try:
            self.graph = Graph(connection, auth=(username, password))
            print('Successful connection')
        except Exception:
            print("Error, failed to connect")
            exit(1)

    def change_connection(self, connection, username, password):
        try:
            self.graph = Graph(connection, auth=(username, password))
            print('Successful connection')
        except Exception:
            print("Error, failed to connect")

    
    def find_element(self, id):
        tx = self.graph.begin()
        matcher = NodeMatcher(self.graph)
        element = matcher.get(id)
        
        if element is None:
            print('No element was found with this ID number: ' + str(id))
            return None
        
        return element

    def modify_node_properties(self, properties, node = None , id = None):
       
        if node is None and id is not None:
            matcher = NodeMatcher(self.graph)
            node = matcher.get(id)
        
        try:

            for key, value in properties.items():
                node[key] = value
        
            node.push()
        except Exception:
            print('No node was found!')
    
    def modify_edge_properties(self, properties, edge = None , id = None):
           
        if edge is None and id is not None:
            matcher = RelationshipMatcher(self.graph)
            edge = matcher.get(id)
        
        try:

            for key, value in properties.items():
                edge[key] = value
        
            edge.push()
        except Exception:
            print('No node was found!')


    def delete_node(self, id):
        tx = self.graph.begin()
        matcher = NodeMatcher(self.graph)
        element = matcher.get(id)
        try:
            tx.delete(element)
            tx.commit()
            print("Successful deletion")
        except Exception:
            print('No element was found with this ID number: ' + str(id)+ '. Could not delete.')


    def create_node(self, label, properties = {}):

        if len(properties) == 0:
            node = Node(label)
        else:
            node = Node(label, **properties)
        self.graph.create(node)
        return node

    def check_relationship_exist(self, source_node, target_node, label = None, properties = {}):
        matcher = RelationshipMatcher(self.graph)

        if label is not None and len(properties) > 0:
            relationship = matcher.match(nodes=(source_node, target_node), r_type=label, properties=properties).first()
        elif label is None and len(properties) > 0:
            relationship = matcher.match(nodes=(source_node, target_node), properties=properties).first()
        elif label is not None and len(properties) == 0:
            relationship = matcher.match(nodes=(source_node, target_node), r_type=label).first()
        else:
            relationship = matcher.match(nodes=(source_node, target_node)).first()

        return relationship is not None, relationship


    def create_relationship(self, source_node, target_node, label, properties = {}):
        if len(properties) > 0:
            relationship = Relationship(source_node, label, target_node, **properties)
        else:
            relationship = Relationship(source_node, label, target_node)
        self.graph.create(relationship)
        return relationship

    def check_node_exist(self, label = None,properties = None, id = None):
        matcher = NodeMatcher(self.graph)
        if properties is None and id is None and label is None:
            node = None
        elif id is not None:
            node = matcher.get(id)
            
        elif label is not None and properties is not None:
            node = matcher.match(label, **properties).first()
        else:
            node = matcher.match(**properties).first()

        return node is not None, node

    def search_nodes_by_properties(self, label, properties = None, limit = 10):
        matcher = NodeMatcher(self.graph)

        if properties is None:
            list_nodes = matcher.match(label).limit(limit)
        else:
            list_nodes = matcher.match(label, **properties).limit(limit)

        return list_nodes.all()

    
    def check_edges_exist(self, source_node, target_node, label, properties = None, limit = 10):
        matcher = RelationshipMatcher(self.graph)

        if properties is None:
            list_nodes = matcher.match(nodes= (source_node, target_node),r_type =label, properties = properties).limit(limit).all()
        else:
            list_nodes = matcher.match(nodes= (source_node, target_node),r_type =label, properties = properties).limit(limit).all()

        return len(list_nodes) > 0, list_nodes    

if __name__ == '__main__':

    neo = NEO4J()
    neo.connect()
    