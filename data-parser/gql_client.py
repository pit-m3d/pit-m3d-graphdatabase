from gql import gql, Client
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportQueryError
import re
import logging
import uuid
from dateutil import parser
from datetime import datetime

class GQL:

    def __init__(self,url = 'http://0.0.0.0:3003/', username = '', password = '', schema_path = None):
        self.transport = AIOHTTPTransport(url=url)

        if schema_path is None:
            try:
                self.client = Client(transport=self.transport, fetch_schema_from_transport = True)
            except:
                print('error in connection to GraphQL server')

        else:
            with open(schema_path) as f:
                schema_str = f.read()

            try:
                self.client = Client(schema = schema_str)

            except:
                print('error in connection to GraphQL server')



    def mutation_update_node(self,mutation_method, data_dictionary):
        '''
        Calling a mutation by giving the mutation method and the parameters with updated values
        This fuction updates the values of an existing node or can be used for creating a new one

        Arguments:
            - name of the mutation from the schema
            - properties and values of the node type in dictionary format
        '''

        params = self.format_dictionary(data_dictionary)
        query = gql('''
            mutation {
                 '''+mutation_method +'''(
                    '''+ params +'''
                )
                {
                    uuid
                }
            }
        ''')

        try:
            result = self.client.execute(query)[mutation_method]['uuid']
        except TransportQueryError as e:
            #print("There was a problem with the query or the constraint blocked the action.")

            return False, e.errors[0]['message']
        
        return True, result

    def add_uuid(self):
        return str(uuid.uuid4())

    def format_dictionary(self,dictionary):
        '''
        Format a dictionary into a string that matches the graphql query syntax
        For now, some of the properties are dealt separately due to the xml format
        '''

        dictionary['uuid'] = str(uuid.uuid4())

        data_syntax_list = []
        for key, value in dictionary.items():
            key_camel = self.format_propery(key)
            if "Date" in key_camel:
                value = self.format_date(value)

            if ('Age' in key_camel or 'Range' in key_camel) and value == 'na':
                value = "-1"
            if key_camel == 'indicator':
                value = '"'+str(value.strip())+'"'
            else:
                value = self.format_value(value)
            data_syntax_list.append(f'{key_camel}: '+str(value))

        data = "\n".join(data_syntax_list)
        return data

    def format_date(self, date):
        '''
        Format the date values in the raw xml file.
        The return value is a dictionary with the value beween quotation mark.
        This dictionary will be converted to string to add into the query.
        '''
        result_format = "%Y-%m-%dT%H:%M:%S"
        result = parser.parse(date)
        return {'formatted': '"'+result.isoformat() + '"'}


    def format_properies(self,list_properties):
        '''
        Format a list of properties from underscore format to lowerCamelCase format
        '''

        result = []
        for property in list_properties:
            words = property.split('_')
            word_camel = words[0]
            for i in range(1, len(words)):
                word_camel = word_camel + words[i][0].upper() + words[i][1:]
            result.append(''.join(words))

        return result

    def format_value(self,value):
        '''
        Format the dictionary values to match the correct format of the graphql query
        '''
        
        if value is None:
            return '"NA"'
        if isinstance(value, datetime):
            return value
        if isinstance(value, dict):
            return str(value).replace("'", "")
        if isinstance(value, list):
            return str(value).replace("'", '"')
        else:
            try:
                value = int(value.strip())
                return value
            except ValueError:
                is_digit = False

            try:
                value = float(value.strip())
                return value
            except ValueError:
                is_digit = False

            return '"' + value.strip() + '"'

    def format_propery(self,property):
        '''
        Formatting underscored tags to lowercamel case tags

        Example: xml_tag -> xmlTag
        '''

        words = property.split('_')
        word_camel = words[0]
        for i in range(1, len(words)):
            word_camel = word_camel + words[i][0].upper() + words[i][1:]

        return word_camel

    def get_uuid(self, node_label, name):
        '''
        Get a uuid of a node
        If node does not exist, return flag and None
        '''

        query = gql(
            '''
            query{
                %s(name: "%s"){
                    uuid
                }
            }
            ''' % (node_label, name)
        )

        
        result = self.client.execute(query)
        
        if len(result[node_label]) == 0:
            return False, None

        return True, result[node_label][0]['uuid']

    def check_relationsip_exist(self,metabolite_uuid, param):
        '''
        See if the exact same relationship exist for a metabolite node
        it returns a boolean by checking if the Metabolite uuid is returned by the query
        '''

        query = gql('''
            query{
                Metabolite(filter: {ranges: {%s}})
                {
                    name
                    uuid
                }

            }
        ''' % param)

        result = self.client.execute(query)['Metabolite']

        if len(result) == 0:
            return False
        else:
            if result[0]['uuid'] == metabolite_uuid:
                return True

            return False

    def connect_metabolite_disease(self, disease_uuid, metabolite_uuid, properties):
        '''
        Connecting a metabolite with a disease using the schema mutation.
        Arguments:
            - metabolite and disease nodes' uuid
            - properties of the relationship

        Return:

            if no problem happened:
                return a boolean and the uuid
            if there was a problem:
                return a boolean and the error message
        '''

        params = self.format_dictionary(properties)

        if self.check_relationsip_exist(metabolite_uuid, params):
            return False, "Relationship already exists for metabolite uuid "+metabolite_uuid

        query = gql('''
            mutation{
                AddMetaboliteRanges(
                    from: {uuid: "%s"}
                    to: {uuid: "%s"}
                    data: {%s}
                )
                {
                
                    uuid
                    
                }
            }
        ''' % (metabolite_uuid, disease_uuid, params))
        try:
            result = self.client.execute(query)

        except TransportQueryError as e:
            print("There was a problem with the query when connecting relationship of metabolite and disease.")
            print('uuid of metabolite of disease is not found')
            return False, e.errors[0]['message']

        return True, result

    

if __name__ == "__main__":

    gql_server = GQL()