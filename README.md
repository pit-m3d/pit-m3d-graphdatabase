# Graph editor

This monorepo includes various services for interacting with the PIT-M3D graph database.

## Services

- `/apollo` : An apollo server with the `neo4j-graphql-js` plugin to serve a GraphQL endpoint for interacting with the graph database.
- `/neo` : Configuration for the neo4j graph database server.
- `/data-parser` : Tools for parsing XML data into the neo4j graph database.
- `/graph-editor` : A visual graph editor for modifying nodes in the database.
