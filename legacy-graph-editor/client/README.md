# Graph Editor

This is an editor for the Neo4J metabolite graph database.

To run it in dev mode, use docker compose.
`docker-compose up`

Docker will handle all networking for frontend and backend and set up hotreloading for both the client and api.
