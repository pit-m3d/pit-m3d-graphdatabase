import React, { useState } from 'react';
import DataDrawer from './components/DataDrawer/DataDrawer';
import NavBar from './components/NavBar';
import GraphView from './components/Graph/GraphView';
import { Toolbar } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';

function App() {
  const [target, setTarget] = useState(3000);
  const [openData, setOpenData] = useState(false);

  return (
    <div className="App__container">
      <CssBaseline />
      <NavBar
        open={openData}
        handleDrawerOpen={() => {
          setOpenData(true);
        }}
        setTargetID={setTarget}
      />
      {/* Toolbar to prevent things hiding under the navbar */}
      <Toolbar />
      <GraphView targetID={target} setTargetID={setTarget} />
      <DataDrawer
        targetID={target}
        open={openData}
        handleDrawerClose={() => {
          setOpenData(false);
        }}
      />
    </div>
  );
}

export default App;
