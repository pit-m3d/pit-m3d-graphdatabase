import React, { useEffect } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress';
import SmilesDrawer from 'smiles-drawer'

function Results({ metabolites }) {

    useEffect(() => {
        SmilesDrawer.apply();
    })

    if (!metabolites) {
        return <CircularProgress/>
    } else {

        const items = metabolites.map(result => {
            return (
                <li key={result.name}>
                    <h4>{result.name}</h4>
                    <canvas data-smiles={result.smiles}></canvas>
                </li>
            )
        })
        return (
            <div>
                <ol>
                    {items}
                    <br/>
                </ol>
                { SmilesDrawer.apply() }
            </div>
        )
    }

}

export default Results;
