import './GraphView.css';

import React, { useEffect } from 'react';
import { blue, green, purple, red } from '@material-ui/core/colors';

import cytoscape from 'cytoscape';
import graphLayout from './graphLayout';
import stylesheet from './stylesheet';

function GraphView({ targetID, setTargetID }) {
  const container = React.useRef(null);
  const graph = React.useRef();

  const nodeMenuConfig = {
    selector: 'core',
    commands: [
      {
        content: 'Add Metabolite',
        fillColor: green[800],
        select: async (ele) => {
          try {
            const res = await fetch(
              encodeURI(`/metabolites/create/HMDBMetabolite`)
            );
            const newID = await res.json();
            console.log(`Created new node with id ${newID}`);
            setTargetID(newID);
          } catch (error) {
            console.error(error);
          }
        },
      },
      {
        content: 'Add Disease',
        fillColor: green[800],
        select: async (ele) => {
          try {
            const res = await fetch(
              encodeURI(`/metabolites/create/HMDBDisease`)
            );
            const newID = await res.json();
            console.log(`Created new node with id ${newID}`);
            setTargetID(newID);
          } catch (error) {
            console.error(error);
          }
        },
      },
    ],
  };

  // prettier-ignore
  function createGraph() {
    try {
      // Create the graph
      graph.current = cytoscape({
        elements: fetch(`/metabolites/graph/${targetID}`).then((res) =>
          res.json()
        ),
        style: stylesheet,
        container: container.current,
        layout: graphLayout,
      });

      const nodeMenu = graph.current.cxtmenu(nodeMenuConfig);

      // Register event listeners
      graph.current.on('tap', 'node', (e) => setTargetID(e.target.data('id')));

    } catch (error) {
      console.error(error);
      createGraph()
    }
  }

  // TargetID update
  useEffect(() => {
    if (graph.current) {
      graph.current.destroy();
    }
  }, [targetID]);

  // Graph init
  useEffect(() => {
    if (!container.current) {
      return;
    }
    if (!graph.current) {
      createGraph();
    } else if (graph.current.destroyed()) {
      createGraph();
    }
  });
  return <div id="cy" ref={container} />;
}

export default GraphView;
