const graphLayout = {
  name: 'concentric',
  directed: true,
  infinite: true,
  fit: true,
  animate: true,
  nodeDimensionsIncludeLabels: false,
  padding: 100,
  minNodeSpacing: 50,
};
export default graphLayout;
