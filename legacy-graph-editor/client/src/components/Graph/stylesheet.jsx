import { blue, green, grey, red } from '@material-ui/core/colors';

const stylesheet = [
  {
    selector: 'node',
    style: {
      'background-color': '#43447a',
      'transition-property': 'background-color border-color',
      'transition-duration': '0.3s',
      'transition-timing-function': 'ease-in-sine',
      label: 'data(name)',
      'font-family': 'Roboto',
      'font-size': 10,
      'text-transform': 'lowercase',
      'text-wrap': 'wrap',
      'background-fit': 'none',
      'background-image-opacity': 1.0,
      'background-width': '80%',
      'background-height': '80%',
    },
  },
  {
    selector: "[type = 'HMDBMetabolite']",
    style: {
      'background-opacity': 0,
      'background-image': `${process.env.PUBLIC_URL}/icons/metabolite.svg`,
    },
  },
  {
    selector: "[type = 'HMDBDisease']",
    style: {
      'background-opacity': 0,
      'background-image': `${process.env.PUBLIC_URL}/icons/virus.svg`,
    },
  },
  {
    selector: "[type = 'INCREASED_RISK_OF']",
    style: {
      'line-color': green[800],
      'target-arrow-color': green[800],
    },
  },
  {
    selector: "[type = 'DECREASED_RISK_OF']",
    style: {
      'line-color': red[800],
      'target-arrow-color': red[800],
    },
  },
  {
    selector: "[type = 'SAME_RISK_OF']",
    style: {
      'line-color': grey[800],
      'target-arrow-color': grey[800],
    },
  },
  {
    selector: "[type = 'UNKNOWN_RISK_OF']",
    style: {
      'line-color': blue[800],
      'target-arrow-color': blue[800],
    },
  },
  {
    selector: 'edge',
    style: {
      width: 9,
      'target-arrow-shape': 'triangle',
      label: 'data(type)',
      'font-family': 'Roboto',
      'font-size': 6,
      color: 'white',
      'curve-style': 'bezier',
      'text-rotation': 'autorotate',
    },
  },
];
export default stylesheet;
