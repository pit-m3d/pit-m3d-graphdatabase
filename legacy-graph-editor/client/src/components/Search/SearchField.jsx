import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  CircularProgress,
  TextField,
  InputAdornment,
  Tooltip,
  SvgIcon,
} from '@material-ui/core';
import Autosuggest from 'react-autosuggest';
import SmilesDrawer from 'smiles-drawer';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import './SearchField.css';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
    minWidth: 200,
    color: theme.palette.common.white,
    // backgroundColor: theme.palette.common.white,
  },
}));

function SearchField({ index, setTargetID }) {
  const [suggestions, setSuggestions] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState('');

  const classes = useStyles();

  const smilesDrawer = new SmilesDrawer.SvgDrawer({});

  return (
    <Autosuggest
      suggestions={suggestions}
      onSuggestionsFetchRequested={() => {
        if (value.length < 1) {
          return;
        }

        setLoading(true);
        // Fetch the suggestions
        (async () => {
          console.log(index, value);
          const res = await fetch(
            encodeURI(`/metabolites/search?index=${index}&query=${value}`)
          );
          const data = await res.json();
          console.log(data);
          setSuggestions(data);
        })();
        setLoading(false);
      }}
      onSuggestionsClearRequested={() => {
        setSuggestions([]);
      }}
      getSuggestionValue={(suggestion) => {
        return suggestion.name;
      }}
      inputProps={{
        label: 'Search for a node...',
        value,
        onChange: (event, { newValue }) => {
          setValue(newValue);
        },
      }}
      onSuggestionSelected={(event, { suggestion }) => {
        console.log(suggestion.id);
        setTargetID(suggestion.id);
      }}
      renderInputComponent={(inputProps) => {
        return (
          <TextField
            {...inputProps}
            className={classes.margin}
            variant="outlined"
            color="primary"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  {loading ? (
                    <CircularProgress />
                  ) : (
                    <ClearIcon
                      onClick={() => {
                        setValue('');
                        setSuggestions([]);
                      }}
                    />
                  )}
                </InputAdornment>
              ),
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
          />
        );
      }}
      renderSuggestion={(suggestion) => {
        return <span>{suggestion.name}</span>;
      }}
    />
  );
}

export default SearchField;
