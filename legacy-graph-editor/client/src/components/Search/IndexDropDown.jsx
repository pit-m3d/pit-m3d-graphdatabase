import {
  FormHelperText,
  MenuItem,
  Select,
  CircularProgress,
} from '@material-ui/core';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  indexSelect: {
    margin: theme.spacing(1),
    minWidth: 200,
    color: 'white',
  },
}));

function IndexDropDown({ index, setIndex }) {
  const classes = useStyles();

  const [options, setOptions] = React.useState([]);
  return (
    <Select
      labelId="index-select"
      id="index-select"
      className={classes.indexSelect}
      value={index}
      placeholder="Search for..."
      onChange={(event) => {
        setIndex(event.target.value);
      }}
      onOpen={async () => {
        setOptions([]);
        const res = await fetch('/metabolites/list_indexes');
        const data = await res.json();
        setOptions(data);
      }}
    >
      {options.length ? (
        options.map((option) => {
          return (
            <MenuItem key={option.name} value={option.name}>
              {option.name}
            </MenuItem>
          );
        })
      ) : (
        <MenuItem value="" disabled style={{ justifyContent: 'center' }}>
          <CircularProgress
            style={{
              width: 24,
              height: 24,
            }}
          />
        </MenuItem>
      )}
    </Select>
  );
}

export default IndexDropDown;
