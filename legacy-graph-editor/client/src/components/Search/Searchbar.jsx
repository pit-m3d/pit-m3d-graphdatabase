import React from 'react';
import { CircularProgress, FormControl } from '@material-ui/core';

import IndexDropDown from './IndexDropDown';
import SearchField from './SearchField';

function Searchbar({ setTargetID }) {
  const [index, setIndex] = React.useState('');

  return (
    <FormControl
      variant="outlined"
      required
      style={{
        display: 'inline-flex',
        flexDirection: 'row',
        alignItems: 'center',
      }}
    >
      {index ? (
        <SearchField index={index} setTargetID={setTargetID} />
      ) : (
        'Please choose an index.'
      )}
      <IndexDropDown index={index} setIndex={setIndex} />
    </FormControl>
  );
}

export default Searchbar;
