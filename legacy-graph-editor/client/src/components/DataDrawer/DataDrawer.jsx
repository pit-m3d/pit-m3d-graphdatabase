import { makeStyles, useTheme } from '@material-ui/core/styles';

import AddIcon from '@material-ui/icons/Add';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import PropertyEditor from './PropertyEditor';
import React from 'react';
import Typography from '@material-ui/core/Typography';

const drawerWidth = '50%';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: 0,
  },
  tableContainer: {
    height: '100%',
  },
  table: {},
}));

function DataDrawer({ open, handleDrawerClose, targetID }) {
  const classes = useStyles();
  const theme = useTheme();

  const [properties, setProperties] = React.useState([]);
  const [listUpdate, setListUpdate] = React.useState(false);

  React.useEffect(() => {
    if (listUpdate) {
      setListUpdate(false);
    }
    (async () => {
      const res = await fetch(encodeURI(`/metabolites/id/${targetID}`));
      const data = await res.json();
      setProperties(data);
    })();
  }, [targetID, listUpdate]);

  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="right"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
          <Typography variant="h6" noWrap className={classes.title}>
            Properties
          </Typography>
          <IconButton
            onClick={() => {
              setProperties([
                ...properties,
                { key: 'new_property', value: 'new_value' },
              ]);
            }}
          >
            <AddIcon />
          </IconButton>
        </div>
        <Divider />
        {properties.length ? (
          <PropertyEditor
            targetID={targetID}
            properties={properties}
            setListUpdate={setListUpdate}
          />
        ) : (
          <Typography variant="h6" noWrap>
            Please select a node
          </Typography>
        )}
      </Drawer>
    </div>
  );
}

export default DataDrawer;
