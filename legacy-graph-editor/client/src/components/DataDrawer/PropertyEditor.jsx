import { CircularProgress, List } from '@material-ui/core';

import Property from './Property';
import React from 'react';

function PropertyEditor({ targetID, properties, setListUpdate }) {
  return (
    <List>
      {properties.length ? (
        properties.map((property) => {
          return (
            <Property
              targetID={targetID}
              targetProperty={property}
              key={property.key}
              setListUpdate={setListUpdate}
            />
          );
        })
      ) : (
        <CircularProgress
          style={{
            alignSelf: 'center',
          }}
        />
      )}
    </List>
  );
}

export default PropertyEditor;
