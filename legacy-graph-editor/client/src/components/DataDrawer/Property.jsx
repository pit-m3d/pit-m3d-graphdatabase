import {
  CircularProgress,
  IconButton,
  ListItem,
  TextField,
  Tooltip,
} from '@material-ui/core';

import DeleteIcon from '@material-ui/icons/Delete';
import React from 'react';
import SaveIcon from '@material-ui/icons/Save';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(1),
  },
  icon: {
    margin: theme.spacing(1),
  },
}));

function Property({ targetID, targetProperty, setListUpdate }) {
  const classes = useStyles();

  const [key, setKey] = React.useState(targetProperty.key || '');
  const [value, setValue] = React.useState(targetProperty.value || '');
  const [unsavedChanges, setUnsavedChanges] = React.useState(false);
  const [saving, setSaving] = React.useState(false);

  React.useEffect(() => {
    setKey(targetProperty.key);
    setValue(targetProperty.value);
  }, [targetProperty]);

  React.useEffect(() => {
    if (targetProperty.key !== key || targetProperty.value !== value) {
      setUnsavedChanges(true);
    } else {
      setUnsavedChanges(false);
    }
  }, [key, value, targetProperty]);

  return (
    <ListItem className="Property">
      <TextField
        className={classes.root}
        label={`Property${unsavedChanges ? '(Edited)' : ''}`}
        value={key}
        onChange={(event) => {
          setKey(event.target.value);
        }}
        fullWidth
        disabled={saving}
        variant="outlined"
      />
      <TextField
        className={classes.root}
        label={`Value${unsavedChanges ? '(Edited)' : ''}`}
        value={value}
        onChange={(event) => {
          setValue(event.target.value);
        }}
        multiline
        rows={4}
        fullWidth
        disabled={saving}
        variant="outlined"
      />
      <IconButton
        edge="end"
        aria-label="save"
        onClick={async (event) => {
          setSaving(true);
          const res = await fetch(
            encodeURI(`/metabolites/editnode/${targetID}/${key}/${value}`),
            {
              method: 'POST',
            }
          );
          if (res.status === 200) {
            console.log(res.status);
            setUnsavedChanges(false);
          } else {
            console.error(res.status);
          }
          setSaving(false);
        }}
      >
        {saving ? (
          <CircularProgress style={{ width: '24px', height: '24px' }} />
        ) : (
          <Tooltip title="Save changes">
            <SaveIcon className={classes.icon} />
          </Tooltip>
        )}
      </IconButton>
      <IconButton
        edge="end"
        aria-label="delete"
        disabled={saving}
        onClick={(event) => {
          const res = fetch(
            encodeURI(`/metabolites/delete/${targetID}/${key}`),
            {
              method: 'POST',
            }
          );
          if (!res.ok) {
            console.error(res.status);
          }
          setListUpdate(true);
        }}
      >
        <Tooltip title="Delete property">
          <DeleteIcon className={classes.icon} />
        </Tooltip>
      </IconButton>
    </ListItem>
  );
}

export default Property;
