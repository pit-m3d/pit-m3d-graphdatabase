"""Application Factory."""

from flask import Flask
import os


def create_app(test_config=None) -> None:
    """Create and configure the app."""
    # CONFIG
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev'  # TODO: Flask secret. Change in prod!
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # BLUEPRINTS
    from .endpoints import metabolites
    app.register_blueprint(metabolites.bp)

    # ROUTES
    @app.route('/status')
    def status():
        return '👋', 200

    return app


