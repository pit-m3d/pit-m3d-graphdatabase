"""API bp."""

import json
import os
import logging

from flask import Blueprint, request, redirect, url_for, current_app
from flask.helpers import make_response
from py2neo import Graph, Node, NodeMatcher, RelationshipMatcher, Relationship

# CONFIG
bp = Blueprint('metabolites', __name__, url_prefix='/metabolites')
g = Graph(
    os.getenv('NEO_URI','bolt://127.0.0.1'),
    auth=('neo4j', 'test')
)

# Get a single propery of a node by ID
@bp.route('/property/<id>/<property>', methods=['GET'])
def get_property(id, property):
    """Get a node's property value by its ID and property name.

        Args:
            id (int): node id number
            property (string): property key

        Returns:
            list of dictionaries with property value in it
    """

    if id.startswith('r'):
        id = int(id[1:])
        response = g.run(
        cypher=f"""
            MATCH (n)-[r]-() WHERE ID(r) = $id RETURN r.{property} as {property}
        """, id = id)
    else:
        response = g.run(
        cypher=f"""
            MATCH (n) WHERE ID(n) = {id} RETURN n.{property} as {property}
        """)

    return json.dumps(response.data()), 200


#List out indexes' name in the database
@bp.route('/list_indexes', methods=['GET'])
def list_indexes():
    """List out possible search options

        Returns:
            list of dictionaries
    """

    response = g.run(
        cypher="""
            CALL db.indexes()
        """)

    data = [{'name': 'Metabolite'}, {'name' : 'Disease'}]

    return json.dumps(data), 200


# Get all the properties of a node found by ID
@bp.route('/id/<id>', methods = ["GET"])
def get_all_properties(id):
    """Get all the properties of a node selected by id

        Args:
            id (int): node's id number

        Returns:
            list of dictionaries with all the key name and values in it.
    """
    if id.startswith('r'):
        id = int(id[1:])
        response = g.run(
            cypher = f"""
            MATCH (r)-[n]-(p) where ID(n) = $id return n
            """, id = id)
    else:
        id = int(id)
        response = g.run(
            cypher = f"""
            MATCH (n) where ID(n) = $id return n
            """, id = id)
    result = [{'key': k, 'value': v} for (k,v) in response.data()[0]['n'].items()]
    return json.dumps(result), 200


# Search nodes by name and synonym (diease or metabolite)
@bp.route('/search', methods=['GET'])
def search():
    """Search method for node

        Retruns:
            list of dictionaries with information of the nodes in it
    """

    index_dictionary = {'Metabolite': 'metabolite_name_accession', 'Disease': 'disease_name'}
    query = request.args.get('query')
    index = index_dictionary[request.args.get('index', default='Metabolite')]

    if query is None or index is None:
        return {}, 200

    current_app.logger.debug(request.args.get('query'))
    response = search_query(index, query)
    return json.dumps(response.data()), 200

# Delete node by ID
@bp.route('/delete/<id>', methods = ['GET'])
def delete(id):
    """Delete the node by id

        Args:
            id (int): node id

        Returns:
            None
    """
    try:
        if id.startswith('r'):
            id = int(id[1:])

            g.evaluate(
            cypher=f"""
                MATCH (n)-[r]-() WHERE ID(r) = {id} DETACH DELETE r
            """)
        else:
            id = int (id)
            g.evaluate(
            cypher=f"""
                MATCH (n) WHERE ID(n) = {id} DETACH DELETE n
            """)
    except Exception:

        return 'No element was found with this ID number: ' + str(id)+ '. Could not delete.', 404

    return 'Successful deletion', 200

#Create node
@bp.route('/create/<label>', methods = ['POST', 'GET'])
def create_node(label):
    """Create a node by getting the properties from json

        Args:
            label (str): node label
        Returns
            node_id (int):
        Raises:
            If name is not given or node iwth exact name already exist
    """
    tx = g.begin()
    node = Node(label)
    node['name'] = 'NEW UNNAMED NODE'
    g.create(node)


    return json.dumps(node.identity)

#Create relationship between two nodes
@bp.route('/create_rel/<type>/<source_id>/<target_id>', methods = ['GET'])
def create_relationship(type, source_id,target_id):
    """Create a relationship between two nodes by inputing their id's
        rel_type is the key for the relationship type

        Args:
            source_id (int): id of source node
            target_node (int): id of target node

        Returns:
            None
    """

    matcher = NodeMatcher(g)
    source_node = matcher.get(int(source_id))
    target_node = matcher.get(int(target_id))

    relationship = Relationship(source_node,type, target_node)
    try:
        g.create(relationship)
    except:
        print('Could not add relationship.')
        return 'Could not add relationship.', 404

    return 'Added relationship.', 200

#Edit relationship
@bp.route('/editrel/<id>/<property_key>/<property_value>', methods = ['GET'])
def edit_relationship(id, property_key, property_value):
    """Given an id, key name and key value, update the relationship

        Args:
            id (int): id of the relationship
            property_key (str): name of the property key
            property_value (str/int): value of the property
        Retruns:
            None
    """
    id = int(id[1:])
    relationship = g.evaluate("MATCH (n)-[r]-() where ID(r) =$id return r LIMIT 1", id = id)
    relationship[property_key] = property_value

    g.push(relationship)

    return 'Propertiy for relationship is updated', 200


@bp.route('/cypher', methods = ['GET'])
def cypher_query():
    """Execute a cypher query and return the result
    """

    query = request.args.get('cypher')
    result = g.run(cypher=query)
    data = result.data()
    response = {'response' : data}

    return response

@bp.route('/delete/<id>/<property_key>', methods = ['POST'])
def remove_node_property(id, property_key):
    """Remove property of a node

        Args:
            id (int): node id
            property_key (str): property name
        Returns:
            None
    """

    matcher = NodeMatcher(g)
    node = matcher.get(id)
    try:
        del node[property_key]
    except:
        print('Key does not exist')
        return 'Key does not exist', 404

    g.push(node)
    return 'Property was removed successfully', 200

@bp.route('/delete_rel/<id>/<property_key>', methods = ['GET'])
def remove_relationship_property(id, property_key):
    """Remove property of a relationship

        Args:
            id (int): node id
            property_key (str): property name
        Returns:
            None
    """
    id = int(id[1:])
    matcher = RelationshipMatcher(g)
    relationship = matcher.get(id)

    try:
        del relationship[property_key]
    except KeyError:
        print('key does not exist')
        return 'key does not exist', 404

    g.push(relationship)
    return 'Key was removed successfully', 200

#Edit node
@bp.route('/editnode/<id>/<property_key>/<property_value>', methods = ['POST'])
def edit_node(id, property_key, property_value):
    """Add or edit property of a node

        Args:
            id (int): node id
            property_key (str): property key/name
            property_value (str): value of the key
        Returns:
            None
    """

    node = g.evaluate(f"MATCH (n) where ID(n) ={id} return n LIMIT 1")
    node[property_key] = property_value

    g.push(node)

    return 'Node property is edited', 200

#Return adjacent nodes of a selected node and their relationships
@bp.route('/graph/<id>', methods=['GET'])
def graph(id):
    """Given a node, return the basic information of neighbouring nodes and edges/relationships

        Args:
            id (int): node id
        Return:
            List of dictionaries representing nodes or relationships with information in them
    """
    # Spec for cytoscape graph format: https://js.cytoscape.org/#notation/elements-json

    response = []

    # Get the metabolite
    cypher = """
        MATCH (n)
        WHERE ID(n) = $id and not n.name = 'NORMAL' and EXISTS(n.name)
        RETURN { id: ID(n), name: n.name, smiles: n.smiles, type: head(labels(n)) } as data
        """
    metabolite = g.run(cypher=cypher, id=int(id))

    response.extend(metabolite.data())

    #Get adjacent nodes
    cypher = """
        match (n)--(p)
        where ID(n) = $id and not p.name = 'NORMAL' and EXISTS(p.name) and head(labels(p)) <> 'HMDBSynonym'
        return {id: ID(p), name: p.name, smiles: p.smiles, type: head(labels(p))} as data
    """

    rels = g.run(cypher=cypher, id=int(id))
    response.extend(rels.data())

    # Get the relationships
    cypher = """
        MATCH (n)-[r]-(d)
        WHERE ID(n) = $id and not n.name = 'NORMAL' and EXISTS(n.name)
            and not d.name = 'NORMAL' and EXISTS(d.name) and type(r) <> 'SYNONYM'
        RETURN { id: 'r' + ID(r), source: ID(n), target: ID(d), type: TYPE(r) } as data
        """

    rels = g.run(cypher=cypher, id=int(id))
    response.extend(rels.data())


    return json.dumps(response), 200


@bp.route('/status')
def status():
    return '👋 from metabolite endpoint', 200


@bp.errorhandler(500)
def server_error(error):
        return 'Looks like something went wrong, did you use the right query parameters? 😟', 500

#This is a helper function for the search method
def search_query(index, query):
    """This is a helper function for search method

        Args:
            index (str): name of the index being used
            query (str): the query being searched
        Returns:
            list of dictionaries with all the search results and their basic properties
    """

    cypher = """
        call db.index.fulltext.queryNodes($index, toUpper($query)) yield node, score
            with node, score match(node)
            return ID(node) as id, node.smiles as smiles, node.name as name,
            head(labels(node)) as type, node.accession as accession
        union
        call db.index.fulltext.queryNodes($index, toUpper($query)+'~') yield node, score
            with node, score match(node)
            return ID(node) as id, node.smiles as smiles, node.name as name,
            head(labels(node)) as type, node.accession as accession order by score desc limit 12
    """

    if "metabolite" in index.lower():
        metabolite_synonym = """call db.index.fulltext.queryNodes("metabolite_synonym", toUpper($query)) yield node, score
            with node, score match(node)-[:SYNONYM]-(meta) return ID(meta) as id,
            meta.smiles as smiles, meta.name as name, head(labels(meta)) as type, meta.accession as accession"""

        metabolite_synonym_fuzzy = """call db.index.fulltext.queryNodes("metabolite_synonym", toUpper($query)+'~') yield node, score
            with node, score match(node)-[:SYNONYM]-(meta) return ID(meta) as id,
            meta.smiles as smiles, meta.name as name, head(labels(meta)) as type, meta.accession as accession order by score desc limit 12"""



        cypher = " union ".join([cypher, metabolite_synonym, metabolite_synonym_fuzzy])
    result = g.run(cypher=cypher, query=query, index = index)
    return result
