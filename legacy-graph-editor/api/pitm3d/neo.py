"""Connection to Neo4J."""
from py2neo import Graph, Node, Relationship
from typing import List

graph = Graph(
    'bolt://192.168.0.98',
    auth=('neo4j', 'test')
)


def find_metabolites(query: str, count: int) -> List[dict]:
    """Find metabolites through fulltext search."""

    metabolites =
