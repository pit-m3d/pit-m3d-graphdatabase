import { assertSchema, makeAugmentedSchema } from 'neo4j-graphql-js';

import { ApolloServer } from 'apollo-server';
import fs from 'fs';
import neo4j from 'neo4j-driver';
import path from 'path';

// Load type definitions file.
const typeDefs = fs
  .readFileSync(
    process.env.GRAPHQL_SCHEMA ||
      path.join(__dirname, 'type-definitions.graphql')
  )
  .toString('utf-8');

// Generate a GraphQL Schema from the type definitions.
const schema = makeAugmentedSchema({
  typeDefs,
  config: {
    query: true,
    mutation: true,
  },
});

// Create neo4j server connection driver.
const NEO_URI = process.env.NEO_URI || 'bolt://localhost:7687'; //change this
const driver = neo4j.driver(
  NEO_URI,
  neo4j.auth.basic(
    process.env.NEO4J_USER || 'neo4j',
    process.env.NEO4J_PASS || 'test'
  )
);

process.env.ASSERT_SCHEMA
  ? assertSchema({ schema, driver, debug: true })
  : console.log(`Skipping schema assertion...`);

// Create the Apollo server, with the augmented schema.
const server = new ApolloServer({ schema, context: { driver: driver } });

// Start the Apollo server to listen on the specified port and host.

const PORT = process.env.PORT || 3003;
const IP = process.env.IP || '127.0.0.1';

server.listen(PORT, IP).then(({ url }) => {
  console.log(`GraphQL API ready at ${url}`);
});
