import React, { useState } from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import Viewport from './components/Viewport/Viewport';

// import DataDrawer from './components/DataDrawer/DataDrawer';
// import GraphView from './components/Graph/GraphView';
// import NavBar from './components/NavBar';
// import { Toolbar } from '@material-ui/core';

function App() {
  return (
    <div className="App__container">
      <CssBaseline />
      <Viewport />
    </div>
  );
}

export default App;
