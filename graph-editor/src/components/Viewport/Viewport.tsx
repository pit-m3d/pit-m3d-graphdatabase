import { gql, useQuery } from '@apollo/client';

import { Core } from 'cytoscape';
import CytoscapeComponent from 'react-cytoscapejs';
import React from 'react';

const QUERY = gql`
  query GetNodes($n: Int!) {
    data: Metabolite(first: $n) {
      id: uuid
      label: name
      ranges {
        id: uuid
        label: indicator
        disease: Disease {
          id: uuid
          label: name
        }
      }
    }
  }
`;

function Viewport() {
  const cyRef = React.useRef<Core>();
  const { loading, data } = useQuery(QUERY, { variables: { n: 10 } });

  React.useEffect(() => {
    if (cyRef) {
      cyRef.current?.on('tap node', (event) => {
        console.log(event.target.data('id'));
      });
    }
    // Else: cy is not yet mounted
  }, [cyRef]);
  return loading ? (
    <p>Loading...</p>
  ) : (
    <CytoscapeComponent
      elements={data.data.map((item: any) => {
        const result: any[] = [];
        result.push({ data: { id: item.id, label: item.label } });
        // result.concat(
        //   item.ranges.flatMap((range: any) => {
        //     result.concat([
        //       {
        //         data: { id: range.disease.id, label: range.disease.label },
        //       },
        //     ]);
        //     return {
        //       data: {
        //         id: range.id,
        //         source: item.id,
        //         target: range.disease.id,
        //       },
        //     };
        //   })
        // );
        console.log(result);
        return result;
      })}
      style={{
        width: '100%',
        height: '100%',
        position: 'absolute',
      }}
      layout={{
        name: 'random',
        fit: true,
      }}
      cy={(cy: Core) => {
        cyRef.current = cy;
      }}
    />
  );
}

export default Viewport;
