import 'fontsource-roboto';

import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';

import App from './App';
import React from 'react';
import ReactDOM from 'react-dom';
import cola from 'cytoscape-cola';
import ctxmenu from 'cytoscape-cxtmenu';
import cytoscape from 'cytoscape';
import reportWebVitals from './reportWebVitals';

// Register cytoscape plugins
cytoscape.use(ctxmenu);
cytoscape.use(cola);

const client = new ApolloClient({
  uri: 'http://localhost:3003',
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
